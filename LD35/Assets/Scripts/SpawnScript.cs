﻿using UnityEngine;
using System.Collections;


public class SpawnScript : MonoBehaviour {
	public static float spawnTime = 0.9f;
	private Renderer Rend;
	public GameObject food;

	// Use this for initialization
	void Start () {
		InvokeRepeating("Spawn", 0, spawnTime);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void Spawn () {
		Rend = gameObject.GetComponent<Renderer>();
		var x1 = transform.position.x - Rend.bounds.size.x / 2;
		var x2 = transform.position.x + Rend.bounds.size.x / 2;
		Instantiate (food, new Vector2(Random.Range (x1, x2), transform.position.y), Quaternion.identity);

	}
}
	