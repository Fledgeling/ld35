﻿using UnityEngine;
using System.Collections;

public class FoodScript : MonoBehaviour {
	public SpriteRenderer sr;
	public Sprite[] sprites;
	private float velocityKoef = 1f;
	public static Vector2 foodVel;
	// Use this for initialization
	void Start () {
		foodVel.x = 0; 
		foodVel.y = -5*velocityKoef; 
		sr = GetComponent<SpriteRenderer>();
		sr.sprite = sprites [Random.Range(0,5)];
	}
	
	// Update is called once per frame
	void Update () {
		GetComponent<Rigidbody2D>().velocity = foodVel;
	}


	private void OnTriggerEnter2D(Collider2D coll) {
		Destroy(this.gameObject);
	}

}
