﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TimerScript : MonoBehaviour
{

	public static float time;
    public Text daytext;
    int day;
    public static int phase;
	void Start() {
		time = 10;
        day = 1;
        phase = 1;
        daytext.transform.GetComponent<Text>().text = "Day " + day;
	}
	void Update() {
		time -= Time.deltaTime;

		var minutes = time / 60; //Divide the guiTime by sixty to get the minutes.
		var seconds = time % 60;//Use the euclidean division for the seconds.

        transform.GetComponent<Text>().text = string.Format ("{0:00} : {1:00}", minutes, seconds);
        //"good" end of the day
        if (time <= 0.00 && BowlScript.calories <= MaxCalScript.CalorieGoal) {
            phase++;
            day++;
            daytext.transform.GetComponent<Text>().text = "Day " + day;
            time = 10;
            MaxCalScript.CalorieGoal = MaxCalScript.CalorieGoal - 100;
            BowlScript.calories = 0;
            SpawnScript.spawnTime = SpawnScript.spawnTime - 0.05f;
        }
        //"bad" end of the day
        if (time <= 0.00 && BowlScript.calories >= MaxCalScript.CalorieGoal) {
            day++;
            daytext.transform.GetComponent<Text>().text = "Day " + day;
            time = 10;
            BowlScript.calories = 0;
        }
	}

}