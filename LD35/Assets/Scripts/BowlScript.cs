﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BowlScript : MonoBehaviour {
	public Rigidbody2D r2d;
	public static int calories = 0;
	public static Vector2 bowlVel;
	public Text calCountText;
    int part;

	// Use this for initialization
	void Start () {
		calCountText.text = "Calories: " + calories.ToString ();
	}
	
	// Update is called once per frame
	void Update () {
		float dx = Input.GetAxis("Horizontal");
		bowlVel.x = dx*10;
		GetComponent<Rigidbody2D>().velocity = bowlVel;
		if (transform.position.x <= -8.8f) {
			transform.position = new Vector2(-8.8f, transform.position.y);
		} else if (transform.position.x >= 2f) {
			transform.position = new Vector2(2f, transform.position.y);
		}

	}

	void OnTriggerEnter2D(Collider2D coll) {
		calories = calories + 100;
		calCountText.text = "Calories: " + calories.ToString ();
	}
    public void GetRidOfCallories () {
        part = (BowlScript.calories / 100) * 30;
        BowlScript.calories = BowlScript.calories - part;
        calCountText.text = "Calories: " + calories.ToString ();
    }
}
