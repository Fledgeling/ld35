﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {
	public Text HeroText;
    int phrase;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        phrase = TimerScript.phase;
        switch (phrase) {
            case 1:	{
				StartCoroutine ("SayPhrase");
            }
            break;
            case 2: {
                HeroText.transform.GetComponent<Text>().text = "I feel so strong! I like it. Let's not lose any second and get to work.";
            }
            break;
            case 3: {
                HeroText.transform.GetComponent<Text>().text = "Good job, me! Now - even less calories. What? I don't need that much anyway.";
            }
            break;
            case 4: {
                HeroText.transform.GetComponent<Text>().text = "Finally! I reached the goal! But can I do better? I don't have to eat that much anyway, right?";
            }
            break;
            case 5: {
                HeroText.transform.GetComponent<Text>().text = "They didn't even look at me... Of course they didn't, look at this fat and flab! God, how will I cope it...";
            }
            break;
            case 6: {
                HeroText.transform.GetComponent<Text>().text = "I can't *sob* I can't! I.. I have to eat something. Anything...Get your SHIT together, lady. Do it!";
            }
            break;
            case 7: {
                HeroText.transform.GetComponent<Text>().text = "I'm ok. I'm ok! See, nothing horrible about it. Just a way to control my weight.";
            }
            break;
            case 8: {
                HeroText.transform.GetComponent<Text>().text = "It's so much better now. I can eat what I want. Just, you know, puke and everything is fine. *zzzzz*Wha...what did you say?";
            }
            break;
            case 9: {
                HeroText.transform.GetComponent<Text>().text = "I tell you mom< it's not a problem! I'm good, ok? I can control it and I do. Aren't you happy I am finally fit? Isn't it all you wanted for years? Everyone  wanted. ";
            }
            break;
            case 10: {
                HeroText.transform.GetComponent<Text>().text = "I feel so... lightweight.	...like a feather. ...I'm perfect. Please, help me. I can't handle it.";
            }
            break;
            case 11: {
                HeroText.transform.GetComponent<Text>().text = "GAME OVER";
            }
            break;
            default:break;
        }
	}
    

	IEnumerator SayPhrase()
	{
		HeroText.transform.GetComponent<Text>().text = "Look at yourself, dear.";
		StartCoroutine(WaitForKeyDown(KeyCode.Space));
		HeroText.transform.GetComponent<Text>().text = "You look like a cow! Agh!";
		StartCoroutine(WaitForKeyDown(KeyCode.Space));
		HeroText.transform.GetComponent<Text>().text = "Yeaaah...";
		StartCoroutine(WaitForKeyDown(KeyCode.Space));
		HeroText.transform.GetComponent<Text>().text = "It's time to get in shape.";
		StartCoroutine(WaitForKeyDown(KeyCode.Space));
		yield return null;
	}


	IEnumerator WaitForKeyDown(KeyCode keyCode)
	{
		while (!Input.GetKeyDown(keyCode))
			yield return null;
	}

	IEnumerator Pause()
	{
		FoodScript.foodVel.y = 0;
		BowlScript.bowlVel.x = 0;
		yield return null;
	}
}
